import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "home",
        component: () => import(/* webpackChunkName: "home" */ "@/views/dashboard/viewDashboard.vue"),
    },
    {
        path: "/requests",
        name: "Requests",
        component: () => import(/* webpackChunkName: "invooices" */ "@/views/requests/viewRequests.vue"),
    },
    {
        path: "/invoices",
        name: "Invoices",
        component: () => import(/* webpackChunkName: "invooices" */ "@/views/invoices/viewInvoices.vue"),
    },
    {
        path: "/invoices/invoice",
        name: "Single invoice",
        component: () => import(/* webpackChunkName: "invoice" */ "@/views/invoices/viewInvoice.vue"),
    },
    {
        path: "/test",
        name: "test",
        component: () => import(/* webpackChunkName: "invoice" */ "@/views/test/testcomp.vue"),
    },
];

const router = new VueRouter({
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return {x: 0, y: 0};
        }
    },
    mode: "history",
    base: process.env.BASE_URL,
    routes,
});

export default router;
